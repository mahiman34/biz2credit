import React, { Component } from 'react';
import './style.css';
import { Link } from 'react-router-dom';
import Axios from 'axios';
import { connect } from 'react-redux';
import Edit from './Edit';
import { Spinner } from 'reactstrap';

class EditDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            users: [],
            userId: this.props.match.params.id
        };
    }


    componentDidMount() {
        Axios.get(`http://localhost:8000/api/users2/allUsers/`)
            .then(result => {
                console.log("got dataaaa", result)
                this.setState({
                    users: result.data
                })
            })
            .catch(error => {
                console.log("error", error)

            })
    }


renderSpinner() {
    return (
        <div  >
            <span className="load">Loading.....</span>
            <Spinner type="grow" color="success" className="spinner00" />
        </div>

    )
}

renderUsers() {
    const user = this.state.users.find(x => x._id === this.props.match.params.id);
    const userId = this.props.match.params.id;

    console.log("details", this);
    return (
        <div>
            &nbsp;
             &nbsp;
            <h3 className='edit20'>Update user</h3>
            &nbsp; &nbsp;
            <Edit
                user={user}
                userId={userId}
            />
        </div>
    )
}
renderHome() {
    return (
        this.state.users === undefined ? this.renderSpinner() : this.renderUsers()
    )
}


render() {
    console.log("EDITHOME", this);

    return this.renderHome();

}
}


export default connect(null)(EditDetails);
