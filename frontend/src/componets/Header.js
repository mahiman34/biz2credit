import React, { Component } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Link } from 'react-router-dom';
import './style.css';
import { connect } from 'react-redux';
import Axios from 'axios';
import { withRouter } from "react-router";

class Header extends Component {
    constructor(props) {
        super(props)
        this.state = {

        };
    }

  

    render() {
        console.log('navbar', this);
        return (
            <Navbar expand="lg" className="navbar01 " fixed="top">
                <Navbar.Toggle aria-controls="responsive-navbar-nav" className="navbar-toggler01" />

                <Link to={this.props.userSignin.userInfo ? '/login' : '/home'}  className="home-btn"> List of users</Link>

                <Navbar.Collapse id="responsive-navbar-nav">
                    <div> <Link to='/newUser' className='new'> Create new user</Link></div>
                    {this.props.userSignin.userInfo ? (
                        <div className='box_opt'>
                            <Link to='/profile' className="login-btn">{this.props.userSignin.userInfo.name} </Link>
                            
                        </div>
                    )
                        : (

                            <Link to='/login' className="login-btn">LOGIN</Link>
                        )
                    }
                   
                </Navbar.Collapse>
            </Navbar>
        )
    }
}

const mapStateToProps = (state) => {
    console.log("state", state);
    return {
        userSignin: state.userSignin,
        user: state.user
    }
}

const ShowTheLocationWithRouter = withRouter(Header);

export default connect(mapStateToProps, null)(ShowTheLocationWithRouter);