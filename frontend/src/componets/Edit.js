import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { Spinner } from 'reactstrap';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { withRouter } from "react-router";

class Edit extends Component {
    constructor(props) {
        super(props)

        this.onChangeNewUserName = this.onChangeNewUserName.bind(this);
        this.onChangeNewUserEmail = this.onChangeNewUserEmail.bind(this);
        this.onChangeNewUserPhone = this.onChangeNewUserPhone.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            name: '',
            email: '',
            phone: ''
        }
    }


    onChangeNewUserName(e) {
        this.setState({ name: e.target.value })
    }

    onChangeNewUserEmail(e) {
        this.setState({ email: e.target.value })
    }

    onChangeNewUserPhone(e) {
        this.setState({ phone: e.target.value })
    }

    onSubmit(e) {
        e.preventDefault()

        const newuserObject = {
            name: this.state.name,
            email: this.state.email,
            phone: this.state.phone
        };

        axios.put('http://localhost:8000/api/users2/allUsers/update/' + this.props.userId, newuserObject)
            .then((res) => {
                console.log(res.data)
                console.log('New User successfully updated')
            }).catch((error) => {
                console.log(error)
            })

        this.props.history.push('/home')
    }



    renderSpinner() {
        return (
            <div  >
                <span className="load">Loading.....</span>
                <Spinner type="grow" color="success" className="spinner00" />
            </div>

        )
    }

    renderUsers() {
        const { user } = this.props;
        console.log('editdetails', this);
        return (
            <div className="row">
                <div className="col-sm-4"></div>
                <div className="col-sm-4" id="edit">
                    <div className="form-wrapper">
                        <Form onSubmit={this.onSubmit}>
                            <Form.Group controlId="Name">
                                <Form.Label>Name</Form.Label>
                                <Form.Control type="text" name="name" id="name" placeholder={user.name} value={this.state.name} onChange={this.onChangeNewUserName} />
                            </Form.Group>

                            <Form.Group controlId="Email">
                                <Form.Label>Email</Form.Label>
                                <Form.Control type="email" name="email" id="email" placeholder={user.email} value={this.state.email} onChange={this.onChangeNewUserEmail} />
                            </Form.Group>

                            <Form.Group controlId="Phone">
                                <Form.Label>Mobile No</Form.Label>
                                <Form.Control type="phone" name="phone" id="phone" placeholder={user.phone} maxLength="10" minLength="10" value={this.state.phone} onChange={this.onChangeNewUserPhone} />
                            </Form.Group>

                            <Button variant="danger" size="lg" block="block" type="submit" className='btn waves-effect red lighten-2'>
                                Update User Details
        </Button>
                        </Form>
                    </div>
                </div>
            </div>        )
    }

    renderHome() {
        return (
            this.props.user === undefined ? this.renderSpinner() : this.renderUsers()
        )
    }


    render() {
        console.log("EDITHOME222", this);

        return this.renderHome();

    }
}


export default withRouter(Edit);