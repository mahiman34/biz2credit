import React, { Component } from 'react';
import { Spinner } from 'reactstrap';
import { connect } from 'react-redux';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import axios from 'axios';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import { Link } from 'react-router-dom';


class Users extends Component {
    constructor(props) {
        super(props);
        this.state = {
            users: [],
                   };

    }



    deleteUser(userId) {
        if (window.confirm('Are you sure??')){
            axios.delete('http://localhost:8000/api/users2/allusers/delete/' + userId)
                .then((res) => {
                    console.log('New User successfully deleted!')
                    window.location.reload(false);
                }).catch((error) => {
                    console.log(error)
                })
        }
    }

    componentDidMount() {
        axios.get(`http://localhost:8000/api/users2/allUsers`)
            .then(result => {
                console.log("got data", result)
                this.setState({
                    users: result.data
                })
            })
            .catch(error => {
                console.log("error", error)

            })
    }
    renderSpinner() {
        return (
            <div  >
                <span className="load">Loading.....</span>
                <Spinner type="grow" color="success" className="spinner00" />
            </div>

        )
    }

    renderUsers() {
        return (
            <div className="home container">
                <h4 className='title'> HOME PAGE</h4>
                <Row>
                    {this.state.users.map(user => {
                        return (
                            <Col xl={3} md={4} sm={6}>
                                <Card>
                                    <div className='a name'>Name: </div>
                                    <div >{user.name} </div> &nbsp;
                                    <div className='a email'>Email:  </div>
                                    <div>{user.email} </div> &nbsp;
                                    <div className='a number'>Phone number: </div>
                                    <div>{user.phone}</div> &nbsp;
                                    <div>
                                        <button className='btn-small waves-effect lighten-2 red delete' onClick={() => this.deleteUser(user._id)}><DeleteOutlined /></button>
                                        <Link to={'/edit/' + user._id}><button className='btn-small waves-effect lighten-2 edit right'  ><EditOutlined /></button></Link>
                                    </div>
                                </Card>
                            </Col>
                        )
                    })}
                </Row>
            </div>

        )
    }
    renderHome() {
        return (
            this.state.users === undefined ? this.renderSpinner() : this.renderUsers()
        )
    }


    render() {
        console.log("Homedata", this);

        return this.renderHome();

    }
}

export default connect(null)(Users);