import React from 'react';
import './App.css';
import { connect } from 'react-redux';
import { Switch, Route, Redirect } from 'react-router-dom';
import store from './store/store';
import Login from './componets/login';
import Register from './componets/signup';
import Profile from './componets/profile';
import Users from './componets/Users';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './componets/Header';
import NewUser from './componets/NewUser';
import EditDetails from './componets/EditDetails';
import Edit from './componets/Edit';


class App extends React.Component {
  render() {
    console.log('app', this);
    return (
      <div className='app'>
          <Header/>
          <div className='container-fluid app'>
          <Switch>
             <Route exact path="/login" component={Login} />
             <Route exact path="/register" component={Register} />
             <Route exact path="/profile" component={Profile} />
              <Route exact path="/home">
              {this.props.userSignin.userInfo ? <Users /> : <Redirect to="/login" /> }
              </Route>
            <Route  path="/edit/:id" component={EditDetails} />
              <Route exact path="/newUser" component={NewUser} />
            <Route exact path="/">
              {this.props.userSignin.userInfo ? <Users /> : <Redirect to="/login" />}
            </Route>
          </Switch>
          </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  console.log("state", state);
  return {
    userSignin: state.userSignin,
    user: state.user
  }
}

export default connect(mapStateToProps, null)(App);
