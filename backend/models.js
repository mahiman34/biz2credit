const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const saltRounds = 10;

const userSchema = new mongoose.Schema({
    name: { type: String, required: true, minlength: 3, maxlength: 20},
    email: {
        type: String, required: true, unique: true, index: true, dropDups: true,
    },
    password: { type: String, required: true, minlength:5,  maxlength: 20 },
    isAdmin: { type: Boolean, required: true, default: false },
    phone: {
        type: Number,
        unique: true,
        minlength: 10, maxlength: 10
    }
});

// userSchema.pre('save', function( next ) {
//     var user = this;
    
//     if(user.isModified('password')){    
//         bcrypt.genSalt(saltRounds, function(err, salt){
//             if(err) return next(err);
    
//             bcrypt.hash(user.password, salt, function(err, hash){
//                 if(err) return next(err);
//                 user.password = hash 
//                 next()
//             })
//         })
//     } else {
//         next()
//     }
// });

// userSchema.methods.comparePassword = function (plainPassword, cb) {
//     bcrypt.compare(plainPassword, this.password, (err, isMatch) => {
//         if (err) 
//             return cb(err);
//         else{
//             if(!isMatch)
//                  return cb(null,isMatch);
//             return cb(null,this);
//         }
//     });
// };

const User = mongoose.model('User', userSchema);

module.exports = { User }